//Program to demonstrate Partial Abstraction in C++
#include<iostream>
using namespace std;
class Animal//Abstract class
{
    public:
    //virtual function with implementation
    virtual void Bark()
    {
        cout<<"Bow"<<endl;
    }
    //pure virtual function
    virtual void Walk()=0;
};
class Dog : public Animal
{
    public:
    //Implementation of pure virtual function in derived class
    void Walk()
    {
        cout<<"Walking"<<endl;
    }
};
int main()
{
    Dog obj;
    obj.Bark();
    obj.Walk();
    return 0;
}