#include<iostream>
using namespace std;
class ab
{
  public:
  void displayab()
  {
      cout<<"Welcome ab"<<endl;
  }
};
class abc
{
  public:
  void displayabc()
  {
      cout<<"Welcome abc"<<endl;
  }
};
//Simple Inheritance
class one : public ab
{
    public:
    void displayone()
    {
        cout<<"Hi one"<<endl;
    }
};
//Multiple Inheritance
class two : public ab
{
    public:
    void displaytwo()
    {
        cout<<"Hi 2"<<endl;
    }
};
class three : public one
{
    public:
    void displaythree()
    {
        cout<<"Hi 3"<<endl;
    }
};
class four : public abc
{
    public:
    void displayfour()
    {
        cout<<"Hi 4"<<endl;
    }
};
class five : public abc
{
    public:
    void displayfive()
    {
        cout<<"Hi 5"<<endl;
    }
};
int main()
{
    one c1;
    two c2;
    three c3;
    four c4;
    five c5;
    cout<<"Simple Inheritance"<<endl;
    c1.displayab();
    cout<<"Multiple Inheritance"<<endl;
    c2.displayab();
    c2.displaytwo();
    cout<<"Multilevel Inheritance"<<endl;
    c3.displayone();
    c3.displayone();
    cout<<"Heirarchial Inheritance"<<endl;
    c4.displayfour();
    c4.displayabc();
    c5.displayfive();
    c5.displayabc();
    return 0;
}

