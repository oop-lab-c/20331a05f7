#include <iostream>
using namespace std;
class Animal {
   public:
    void walk() {
        cout << "It can walk" << endl;
    }

    void sleep() {
        cout << "It can sleep" << endl;
    }
};

// derived class
class Dog : public Animal {
 
   public:
    void bark() {
        cout << "It can bark" << endl;
    }
};

int main() {
    // Create object of the Dog class
    Dog puppy;

    // Calling members of the base class
    puppy.walk();
    puppy.sleep();

    // Calling member of the derived class
    puppy.bark();
    return 0;
}
